#include "DES.h"

void menu();

void write_word(std::bitset<64> word, std::ofstream & outFile)
{
    for (int i=0; i<8; i++)
    {
        std::bitset<8> letter;
        for (int j=0; j<8; j++)
            letter[j] = word[i*8+j];
        unsigned char c = letter.to_ulong();
        outFile << c;
    }
}

bool read_word(std::ifstream & inFile, std::bitset<64> & word)
{
    char letter;
    for (int i=0; i<8; i++)
    {
        inFile.get(letter);
        if (inFile.good())  // If the character we read is good, we add it to the bitset
        {
            unsigned char letter2 = letter;
            std::bitset<8> set_letter_bit(letter2);
            for (int j=0; j<8; j++)
                word[8*i+j] = set_letter_bit[j];
        }
        else if (i==0)      // If the first letter of the word is !good, we know we reached eof
            return false;   // already, so we shouldn't write this word
    }
    return true;            // Returns true if the first letter of the word is good
}

void encrypt_file()
{
    std::string inFileName, outFileName;
    std::cout << "Enter the name of the file you would like to encrypt: ";
    //getline(std::cin, inFileName);
    std::cin >> inFileName;
    std::cout << "Enter a name for your encrypted file: ";
    //getline(std::cin, outFileName);
    std::cin >> outFileName;

    unsigned long long k1, k2, k3;
    std::cout << "Enter key 1: ";
    std::cin >> std::hex >> k1;
    std::cout << "Enter key 2: ";
    std::cin >> std::hex >> k2;
    std::cout << "Enter key 3: ";
    std::cin >> std::hex >> k3;
    std::bitset<64> key1(k1);
    std::bitset<64> key2(k2);
    std::bitset<64> key3(k3);

    std::ifstream inFile(inFileName.c_str(), std::ifstream::in);
    std::ofstream outFile(outFileName.c_str(), std::ofstream::out);
    do
    {
        std::bitset<64> word;
        if (read_word(inFile, word))
            write_word(encrypt_triple_DES(word, key1, key2, key3), outFile);
    }
    while (inFile.good());
    inFile.close();
    outFile.close();

    std::cout << "Encryption complete." << std::endl;
    menu();
}

void decrypt_file()
{
    std::string inFileName, outFileName;
    std::cout << "Enter the name of the file you would like to decrypt: ";
    //getline(std::cin, inFileName);
    std::cin >> inFileName;
    std::cout << "Enter a name for your decrypted file: ";
    //getline(std::cin, outFileName);
    std::cin >> outFileName;

    unsigned long long k1, k2, k3;
    std::cout << "Enter key 1: ";
    std::cin >> std::hex >> k1;
    std::cout << "Enter key 2: ";
    std::cin >> std::hex >> k2;
    std::cout << "Enter key 3: ";
    std::cin >> std::hex >> k3;
    std::bitset<64> key1(k1);
    std::bitset<64> key2(k2);
    std::bitset<64> key3(k3);

    std::ifstream inFile(inFileName.c_str(), std::ifstream::in);
    std::ofstream outFile(outFileName.c_str(), std::ofstream::out);
    do
    {
        std::bitset<64> word;
        if (read_word(inFile, word))
            write_word(decrypt_triple_DES(word, key1, key2, key3), outFile);
    }
    while (inFile.good());
    inFile.close();
    outFile.close();

    std::cout << "Decryption complete." << std::endl;
    menu();
}

void menu()
{
    int choice;
    std::cout << std::endl;
    std::cout << "Choose from the following options:" << std::endl;
    std::cout << "1. Encrypt File" << std::endl;
    std::cout << "2. Decrypt File" << std::endl;
    std::cout << "3. Exit Program" << std::endl;
    std::cout << "Choice (1-3):";
    std::cin >> choice;
    while (choice<1 || choice>3)
    {
        std::cout << choice << " is not a valid option." << std::endl
                  << "Please enter a valid choice (1-3): ";
        std::cin >> choice;
    }
    switch(choice)
    {
        case 1:
            encrypt_file();
            break;
        case 2:
            decrypt_file();
            break;
        case 3:
            return;
        default:
            std::cout << "An error has occured. Returning to main menu." << std::endl;
            break;
    }
}

int main()
{
    menu();
    return 0;
}
