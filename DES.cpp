#include "DES.h"
#include "permutation_tables.h"

std::bitset<64> encrypt_triple_DES(std::bitset<64> input, std::bitset<64> key1, std::bitset<64> key2, std::bitset<64> key3)
{
    return encrypt_DES(decrypt_DES(encrypt_DES(input, key1), key2), key3);
}

std::bitset<64> decrypt_triple_DES(std::bitset<64> input, std::bitset<64> key1, std::bitset<64> key2, std::bitset<64> key3)
{
    return decrypt_DES(encrypt_DES(decrypt_DES(input, key3), key2), key1);
}

std::bitset<64> encrypt_DES(std::bitset<64> input, std::bitset<64> key)
{
    initial_permutation(input);
    std::bitset<56> key_seed(PChoice_1(key));
    for (int i=0; i<16; i++)
        rounds(input, key_seed, i, true);
    swap_32bits(input);
    final_permutation(input);
    return input;
}

std::bitset<64> decrypt_DES(std::bitset<64> input, std::bitset<64> key)
{
    initial_permutation(input);
    std::bitset<56> key_seed(PChoice_1(key));
    for (int i=15; i>=0; i--)
        rounds(input, key_seed, i, false);
    swap_32bits(input);
    final_permutation(input);
    return input;
}

void initial_permutation(std::bitset<64> & input)
{
	std::bitset<64> temp;
	for (int i=0; i<64; i++)
		temp[i] = input[IP[i]-1];
	for (int i=0; i<64; i++)
        input[i] = temp[i];
}

void final_permutation(std::bitset<64> & input)
{
	std::bitset<64> temp;
	for (int i=0; i<64; i++)
		temp[i] = input[IIP[i]-1];
	for (int i=0; i<64; i++)
        input[i] = temp[i];
}

std::bitset<48> expansion(std::bitset<32> input)
{
	std::bitset<48> output;
	for (int i=0; i<48; i++)
		output[i] = input[E[i]-1];
	return output;
}

std::bitset<48> whitener(std::bitset<48> input, std::bitset<48> key)
{
	std::bitset<48> output;
	for (int i=0; i<48; i++)
		output[i] = input[i]^key[i];
	return output;
}

std::bitset<32> sBoxes(std::bitset<48> input)
{
	std::bitset<32> output;
	for (int i=0; i<8; i++)
	{
		std::bitset<2> row;
		std::bitset<4> col;
		row[0] = input[6*i];
		row[1] = input[6*i+5];
		for (int j=1; j<=4; j++)
			col[j] = input[6*i+j];
		std::bitset<4> temp (S[i][row.to_ulong()*16+col.to_ulong()]);
		for (int j=0; j<4; j++)
            output[4*i+j] = temp[j];
	}
	return output;
}

std::bitset<32> straight_permutation(std::bitset<32> input)
{
    std::bitset<32> output;
    for (int i=0; i<32; i++)
        output[i] = input[P[i]-1];
    return output;
}

std::bitset<32> F(std::bitset<32> input, std::bitset<48> key)
{
    std::bitset<32> output(straight_permutation(sBoxes(whitener(expansion(input), key))));
    return output;
}

std::bitset<48> key_generation(std::bitset<56> & input, int round_num, bool encrypt)
{
    std::bitset<28> C, D;
    for (int i=0; i<28; i++)
    {
        C[i] = input[i];
        D[i] = input[i+28];
    }
    if (encrypt == true)
    {
        left_shift(C, shifts[round_num]);
        left_shift(D, shifts[round_num]);
        for (int i=0; i<28; i++)
        {
            input[i] = C[i];
            input[i+28] = D[i];
        }
        std::bitset<48> key (PChoice_2(input));
        return key;
    }
    else
    {
        std::bitset<48> key (PChoice_2(input));
        right_shift(C, shifts[round_num]);
        right_shift(D, shifts[round_num]);
        for (int i=0; i<28; i++)
        {
            input[i] = C[i];
            input[i+28] = D[i];
        }
        return key;
    }
}

void left_shift(std::bitset<28> & input, int shift)
{
    std::bitset<2> temp;
    for (int i=28-shift; i<28; i++)
        temp[i-(28-shift)] = input[i];
    for (int i=27; i>=shift; i--)
        input[i] = input[i-shift];
    for (int i=0; i<shift; i++)
        input[i] = temp[i];
}

void right_shift(std::bitset<28> & input, int shift)
{
    std::bitset<2> temp;
    for (int i=0; i<shift; i++)
        temp[i] = input[i];
    for (int i=0; i<28-shift; i++)
        input[i] = input[i+shift];
    for (int i=28-shift; i<28; i++)
        input[i] = temp[i-(28-shift)];
}

std::bitset<56> PChoice_1(std::bitset<64> input)
{
    std::bitset<56> output;
    for (int i=0; i<56; i++)
        output[i] = input[PC1[i]-1];
    return output;
}

std::bitset<48> PChoice_2(std::bitset<56> input)
{
    std::bitset<48> output;
    for (int i=0; i<48; i++)
        output[i] = input[PC2[i]-1];
    return output;
}

void rounds(std::bitset<64> & input, std::bitset<56> & key, int round_num, bool encrypt)
{
    std::bitset<32> L, R, Rout;
    for (int i=0; i<32; i++)
    {
        L[i] = input[i];
        R[i] = input[i+32];
    }
    std::bitset<32> Fout (F(R, key_generation(key, round_num, encrypt)));
    for (int i=0; i<32; i++)
    {
        input[i] = R[i];
        input[i+32] = L[i] ^ Fout[i];
    }
}

void swap_32bits(std::bitset<64> & input)
{
    std::bitset<32> temp;
    for (int i=0; i<32; i++)
    {
        temp[i] = input[i+32];
        input[i+32] = input[i];
    }
    for (int i=0; i<32; i++)
        input[i] = temp[i];
}
