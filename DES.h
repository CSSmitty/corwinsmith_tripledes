#ifndef DES_H_INCLUDED
#define DES_H_INCLUDED

#include <bitset>
#include <iostream>
#include <fstream>
#include <string>

std::bitset<64> encrypt_triple_DES(std::bitset<64>, std::bitset<64>, std::bitset<64>, std::bitset<64>);
std::bitset<64> decrypt_triple_DES(std::bitset<64>, std::bitset<64>, std::bitset<64>, std::bitset<64>);
std::bitset<64> encrypt_DES(std::bitset<64>, std::bitset<64>);
std::bitset<64> decrypt_DES(std::bitset<64>, std::bitset<64>);
void initial_permutation(std::bitset<64> &);
void final_permutation(std::bitset<64> &);
std::bitset<48> expansion(std::bitset<32>);
std::bitset<48> whitener(std::bitset<48>, std::bitset<48>);
std::bitset<32> sBoxes(std::bitset<48>);
std::bitset<32> straight_permutation(std::bitset<32>);
std::bitset<32> F(std::bitset<32>, std::bitset<48>);
std::bitset<48> key_generation(std::bitset<56> &, int, bool = true);
void left_shift(std::bitset<28> &, int);
void right_shift(std::bitset<28> &, int);
std::bitset<56> PChoice_1(std::bitset<64>);
std::bitset<48> PChoice_2(std::bitset<56>);
void rounds(std::bitset<64> & input, std::bitset<56> &, int, bool = true);
void swap_32bits(std::bitset<64> &);
void swap_32bits(std::bitset<64> &);

#endif // DES_H_INCLUDED
