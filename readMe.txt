This program implements Triple-DES algorithm.

This program is made to work on the school computers. To compile the code simply run the command "make" while in the directory.
To run the program use the command './DES' to open the menu.

The program has two options: Encrypt or Decrypt. When selecting an option you will be prompted to input the file you would
like to encrypt/decrypt. This file will need to be in the directory in order to run. You will then be asked what you would
like to name the output file for your encryption/decryption. Lastly, you will enter the three hexadecimal keys that will be
used to decrypt the plaintext/ciphertext.

Files:
The implementation for the DES algorithm can all be found in the DES.cpp file.
The read_file.cpp contains all the functions needed to read/write the encryption/decryption, and holds the main function which runs the menu.
