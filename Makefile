CC=g++
CFLAGS=-Wall -g
OUT=DES
OBJS=DES.o read_file.o

$(OUT): DES.o read_file.o
	$(CC) $(CFLAGS) $(CLIBS) -o $(OUT) $(OBJS)
DES.o: DES.cpp DES.h permutation_tables.h
	$(CC) $(CFLAGS) -c DES.cpp -o DES.o
read_file.o: read_file.cpp DES.h permutation_tables.h
	$(CC) $(CFLAGS) -c read_file.cpp -o read_file.o
.PHONY: clean

clean:
	rm -rf *.o $(OUT)
